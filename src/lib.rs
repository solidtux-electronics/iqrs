use byteorder::{LittleEndian, ReadBytesExt};
use num_complex::Complex;
use std::str::FromStr;

pub enum DataType {
    I16,
    F32,
}

impl DataType {
    pub fn bytes(&self) -> usize {
        match self {
            DataType::I16 => 4,
            DataType::F32 => 8,
        }
    }
}

pub trait ReadComplex<T> {
    type Err;

    fn read_complex(&mut self, d: &DataType) -> Result<Complex<T>, Self::Err>;
}

impl<T> ReadComplex<f32> for T
where
    T: ReadBytesExt,
{
    type Err = std::io::Error;

    fn read_complex(&mut self, d: &DataType) -> Result<Complex<f32>, Self::Err> {
        match d {
            DataType::I16 => {
                let re = self.read_i16::<LittleEndian>()?;
                let im = self.read_i16::<LittleEndian>()?;
                Ok(Complex::new(re as f32, im as f32))
            }
            DataType::F32 => {
                let re = self.read_f32::<LittleEndian>()?;
                let im = self.read_f32::<LittleEndian>()?;
                Ok(Complex::new(re, im))
            }
        }
    }
}

impl FromStr for DataType {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "i16" => Ok(DataType::I16),
            "f32" => Ok(DataType::F32),
            x => Err(format!("Unknown data type {}", x)),
        }
    }
}
